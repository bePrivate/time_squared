# Time^2

Time squared is a psychological test which measures
a subjet's attention span and concentration.

The goal is to click all 25 numbers in the grid in
ascending order as quickly as possible.

After finishing, the subject can see how they performed.

## Installation

_Prerequisites: python3, tkinter, matplotlib  
Refer to their respective installation guides if you require help._

`git clone https://gitlab.com/bePrivate/time_squared`

`cd time_squared`

To start, type `python3 time_squared`.
